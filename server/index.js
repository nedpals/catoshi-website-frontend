const express = require('express');
const ws = require('ws');

const app = express();
const wsServer = new ws.Server({ noServer: true });

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

wsServer.on('connection', socket => {
  setInterval(() => {
    const randCurrent = getRandomInt(100, 1_000_000);
    socket.send(JSON.stringify({ current: randCurrent, max: 1_000_000 }));
  }, 2000);
});

const server = app.listen(3000);

server.on('upgrade', (req, socket, head) => {
  wsServer.handleUpgrade(req, socket, head, socket => {
    wsServer.emit('connection', socket, req);
  });
});
// Vue stuff
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

// Global event hub
import VueEventHub from 'vue-event-hub';

// Inline SVG component
// import VueInlineSvg from 'vue-inline-svg';

// App component
import App from './App.vue';

// Register router and vuex
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueEventHub);

// App store (hah)
import store from './store';

// Components for Router
import Home from './pages/Home.vue';
import Farms from './pages/Farms.vue';
import Governance from './pages/Governance.vue';
import NotFound from './pages/NotFound.vue';
import Faqs from './pages/Faq.vue';

// Router
const router = new VueRouter({
  routes: [
    { path: '/', component: Home },
    { path: '/farms', component: Farms },
    { path: '/governance', component: Governance },
    { path: '/faqs', component: Faqs },
    { path: '*', component: NotFound }
  ]
});

// Render component
new Vue({
  render: h => h(App),
  router,
  store: new Vuex.Store(store)
}).$mount('#app');
import Vue from 'vue/dist/vue';

new Vue({
  el: '#splash-screen',
  data() {
    return {
      success: false,
      email: ""
    }
  },
  computed: {
    emailPlaceholder() {
      return this.success ? "You're subscribed!" : 'Enter email for updates';
    }
  },
  methods: {
    submit() {
      if (this.email.length === 0) return;
      this.email = "";
      this.success = true;

      setTimeout(() => {
        this.success = false;
      }, 2000);
    }
  }
});
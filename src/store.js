// Auth
const wallet = {
  namespaced: true,
  state: {
    active: false 
  },
  mutations: {
    toggleActive(state) {
      state.active = !state.active;
    }
  }
}

const store = {
  modules: {
    wallet
  }
};

export default store;